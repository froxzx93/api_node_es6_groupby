const users = [
    { id: 1, name: 'JOSE' },
    { id: 2, name: 'ALONSO' },
    { id: 3, name: 'MATIAS' },
    { id: 4, name: 'DIEGO' },
    { id: 5, name: 'RODRIGO' },
    { id: 6, name: 'JONA' }
]

const accounts = [
    { id: 1, idUser: 1, currentAmount: 261261, expiredAmount: 0 },
    { id: 2, idUser: 3, currentAmount: 332323, expiredAmount: 23333 },
    { id: 3, idUser: 2, currentAmount: 3323, expiredAmount: 1 },
    { id: 4, idUser: 4, currentAmount: 3233, expiredAmount: 45454 },
    { id: 5, idUser: 6, currentAmount: 97887, expiredAmount: 455 },
    { id: 6, idUser: 5, currentAmount: 44343, expiredAmount: 0 },
    { id: 7, idUser: 5, currentAmount: 434, expiredAmount: 0 },
    { id: 8, idUser: 5, currentAmount: 2221, expiredAmount: 77776 },
    { id: 9, idUser: 1, currentAmount: 12212, expiredAmount: 0 },
    { id: 10, idUser: 4, currentAmount: 434, expiredAmount: 434 },
    { id: 11, idUser: 3, currentAmount: 43434, expiredAmount: 0 },
    { id: 12, idUser: 6, currentAmount: 3323, expiredAmount: 443 }
]


export { users, accounts };