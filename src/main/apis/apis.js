import { groupBySumElement, groupByConcat, createObjectData } from '../commons/commons';
import { users, accounts } from '../../database/data';
import axios from 'axios';

const id = "id";
const idUser = "idUser";
const currentAmount = "currentAmount";
const expiredAmount = "expiredAmount";

const getUsersAmmountTotal = () => {
    const dataObject = groupBySumElement(accounts, idUser, currentAmount);
    return Object.entries(dataObject).map((element) => {
        return { idUser: element[0], ammount: element[1] }
    })
}

const getUserAmmountTotal = (req) => {
    const list = accounts.filter((element) => {
        return element.idUser === parseInt(req.params.id);
    })
    const dataObject = groupBySumElement(list, idUser, currentAmount);
    return {
        idUser: Object.entries(dataObject)[0][0],
        ammount: Object.entries(dataObject)[0][1]
    }
}

const getUsersAccounts = () => {
    const dataObject = groupByConcat(accounts, idUser, id);
    return Object.entries(dataObject).map((element) => {
        return { idUser: element[0], accounts: element[1] }
    })
}

const getUserAccounts = (req) => {
    const list = accounts.filter((element) => {
        return element.idUser === parseInt(req.params.id);
    })
    const dataObject = groupByConcat(list, idUser, id);
    return {
        idUser: Object.entries(dataObject)[0][0],
        accounts: Object.entries(dataObject)[0][1]
    }
}
const getUsersAmountExpired = () => {
    const list = accounts.filter((element) => {
        return element.expiredAmount > 0;
    })
    const dataObject = groupBySumElement(list, idUser, expiredAmount);
    return Object.entries(dataObject).map((element) => {
        return { idUser: element[0], expiredAmount: element[1] }
    })
}

const getUsersNameAmountExpired = async () => {
    const getUsersAmountExpired = await axios.get('http://localhost:3000/getUsersAmountExpired');
    const objectData = createObjectData(users, id);
    const response = getUsersAmountExpired.data.map((element) => {
        return { name: objectData[element[idUser]].name, amount: element.expiredAmount }
    })
    return response;
}

export {
    getUserAmmountTotal, getUsersAmmountTotal, getUsersAccounts, getUserAccounts,
    getUsersAmountExpired, getUsersNameAmountExpired
};