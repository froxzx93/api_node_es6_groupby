import express from 'express';
const app = express();
import {
  getUsersAmmountTotal, getUserAmmountTotal, getUsersAccounts,
  getUserAccounts, getUsersAmountExpired, getUsersNameAmountExpired
} from '../apis/apis';

app.get('/getUsersAmmountTotal', (req, res) => {
  return res.json(getUsersAmmountTotal())
})

app.get('/getUserAmmountTotal/:id', (req, res) => {
  return res.json(getUserAmmountTotal(req))
})
app.get('/getUsersAccounts', (req, res) => {
  return res.json(getUsersAccounts())
})

app.get('/getUserAccounts/:id', (req, res) => {
  return res.json(getUserAccounts(req))
})

app.get('/getUsersAmountExpired', (req, res) => {
  return res.json(getUsersAmountExpired())
})

app.get('/getUsersNameAmountExpired', async (req, res) => {
  const resp = await getUsersNameAmountExpired();
  return res.json(resp);
})
export { app };