const groupBySumElement = (array, key, data) => {
    const resp = array.reduce((acc, value) => {
        const elementId = value[key];
        const elementValue = value[data];
        if (elementId in acc) {
            return { ...acc, [elementId]: parseInt(acc[elementId]) + parseInt(elementValue) }
        } return { ...acc, [elementId]: parseInt(elementValue) }
    }, {})
    return resp;
}

const groupByConcat = (array, key, data) => {
    const resp = array.reduce((acc, value) => {
        const elementId = value[key];
        const elementValue = value[data];
        if (elementId in acc) {
            return { ...acc, [elementId]: acc[elementId].concat(elementValue) }
        } return { ...acc, [elementId]: [elementValue] }
    }, {})
    return resp;
}


const createObjectData = (list, key) => {
    const response = {};
    list.forEach(element => {
        response[element[key]] = element;
    });
    return response;
}

export { groupBySumElement, groupByConcat, createObjectData };