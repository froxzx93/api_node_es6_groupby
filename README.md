Ejecutar :

    npm start

Objetivo :

    Crear APIs

Request (id=1 al 6): 

    http://localhost:3000/getUsersAmmountTotal 
    http://localhost:3000/getUserAmmountTotal/:id 
    http://localhost:3000/getUserAmmountTotal/1
    http://localhost:3000/getUserAmmountTotal/2 
    http://localhost:3000/getUserAmmountTotal/3 

    http://localhost:3000/getUsersAccounts 
    http://localhost:3000/getUserAccounts/:id 
    http://localhost:3000/getUserAccounts/1
    http://localhost:3000/getUserAccounts/2 
    http://localhost:3000/getUserAccounts/3 

    http://localhost:3000/getUsersAmountExpired
    http://localhost:3000/getUsersNameAmountExpired
